package com.example.testing_motionlayout

import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.text.toSpannable
import androidx.databinding.DataBindingUtil
import com.example.testing_motionlayout.databinding.ActivityPptBinding

class PowerPointActivity  : AppCompatActivity() {
    lateinit var mBinding : ActivityPptBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_ppt)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()

        handleLayout()
        transitionListener()
    }

    private fun transitionListener()
    {
        mBinding.mlSlide1.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
                mBinding.rootScene.transitionToState(R.id.to_ppt_2)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })

        mBinding.mlSlide2.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc3) mBinding.rootScene.transitionToState(R.id.to_ppt_3)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
        mBinding.mlSlide3.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc3) mBinding.rootScene.transitionToState(R.id.to_ppt_4)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
        mBinding.mlSlide4.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc3) mBinding.rootScene.transitionToState(R.id.to_ppt_5)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
        mBinding.mlSlide5.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc2Sub4) mBinding.rootScene.transitionToState(R.id.to_ppt_6)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
            }
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {
            }
        })
        mBinding.mlSlide6.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.showTitle) mBinding.rootScene.transitionToState(R.id.to_ppt_7)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
        mBinding.mlSlide7.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc4Sub1) mBinding.rootScene.transitionToState(R.id.to_ppt_8)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
        mBinding.mlSlide8.mlRoot.setTransitionListener(object: MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, id: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, beginStatr: Int, endState: Int) {
                if(beginStatr == R.id.fadeInDesc2Sub1) mBinding.rootScene.transitionToState(R.id.to_ppt_9)
            }
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, startedScene: Int, endScene: Int, p3: Float) {}
        })
    }

    private fun handleLayout()
    {
        val ppt1DescStr = "We use MotionLayout\ninstead of the existing animations".toSpannable()
        handleTestStyle(ppt1DescStr, "MotionLayout")
        mBinding.ppt1Desc.text = ppt1DescStr

        val ppt4Desc1Str = "A subclass of ConstraintLayout (ConstraintLayout 2.0)".toSpannable()
        handleTestStyle(ppt4Desc1Str, "subclass")

        mBinding.mlSlide4.desc1.text = ppt4Desc1Str

        val ppt4Desc2Str = "Was created to bridge the gap between layout transitions and complex motion handling.".toSpannable()
        handleTestStyle(ppt4Desc2Str, "layout transitions")
        handleTestStyle(ppt4Desc2Str, "complex motion")
        mBinding.mlSlide4.desc2.text = ppt4Desc2Str

        val ppt4Desc3Str = "Is intended to move, resize, and animate UI elements with which users interact".toSpannable()
        handleTestStyle(ppt4Desc3Str, "move")
        handleTestStyle(ppt4Desc3Str, "resize")
        handleTestStyle(ppt4Desc3Str, "animate")
        mBinding.mlSlide4.desc3.text = ppt4Desc3Str
    }

    private fun handleTestStyle(spannable: Spannable, styleText: String)
    {
        spannable.setSpan(ForegroundColorSpan(Color.RED), spannable.indexOf(styleText), spannable.indexOf(styleText) + styleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

}