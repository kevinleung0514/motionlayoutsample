package com.example.testing_motionlayout

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.testing_motionlayout.databinding.LayoutDemoBinding

class BeginningActivity : AppCompatActivity() {
    lateinit var mBinding: LayoutDemoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.layout_demo)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()
    }
}