package com.example.testing_motionlayout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewbinding.ViewBinding
import com.example.testing_motionlayout.databinding.ActivityMainBinding
import com.google.android.material.appbar.AppBarLayout

class MainActivity : AppCompatActivity() {
    lateinit var mBinding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()

        mBinding.tvPpt.setOnClickListener {
            startActivity(Intent(MainActivity@this, PowerPointActivity::class.java))
        }

        mBinding.tvDemo.setOnClickListener {
            startActivity(Intent(MainActivity@this, BeginningActivity::class.java))
        }

        mBinding.tvCollapsingToolbar.setOnClickListener {
            startActivity(Intent(MainActivity@this, CollapsingToolbarExampleActivity::class.java))
        }
        mBinding.tvRvAnin.setOnClickListener {
            startActivity(Intent(MainActivity@this, RecyclerViewAnimationExampleActivity::class.java))
        }
        mBinding.tvYoutubeBehavior.setOnClickListener {
            startActivity(Intent(MainActivity@this, YoutubeBehaviorExampleActivity::class.java))
        }
    }
}