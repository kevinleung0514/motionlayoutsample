package com.example.testing_motionlayout.ui

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.recyclerview.widget.RecyclerView

class CustomMotionLayout : MotionLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onNestedPreScroll(target: View, dx: Int, dy: Int, consumed: IntArray, type: Int) {
//        if (!isInteractionEnabled) {
//            return
//        }

        Log.d("ddd", "onNestedPreScroll target $target")

//        val recyclerView = (target as ViewGroup).getChildAt(0)
//        if (target !is RecyclerView) {
//            return super.onNestedPreScroll(target, dx, dy, consumed, type)
//        }
//
//        val canScrollVertically = target.canScrollVertically(-1)
//        if (dy < 0 && canScrollVertically) {
//            // don't start motionLayout transition
//            return
//        }

        super.onNestedPreScroll(target, dx, dy, consumed, type)
    }
}