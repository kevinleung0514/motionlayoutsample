package com.example.testing_motionlayout.data

data class Profile(val name:String, val img:String, val desc:String)
