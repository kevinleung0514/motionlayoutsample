package com.example.testing_motionlayout.adapter

import android.transition.Transition
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testing_motionlayout.R
import com.example.testing_motionlayout.data.Profile
import com.example.testing_motionlayout.databinding.CellRecyclerViewItemBinding


class RecyclerViewAnimAdapter : RecyclerView.Adapter<RecyclerViewAnimAdapter.ViewHolder>() {
    private var mData : ArrayList<Profile> = arrayListOf()
    var animListener: ((View) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),R.layout.cell_recycler_view_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
        (holder.itemView as MotionLayout).addTransitionListener(object : MotionLayout.TransitionListener{
            override fun onTransitionStarted(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int
            ) {
            }

            override fun onTransitionChange(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int,
                progress: Float
            ) {
                Log.d("ddd", "motion onTransitionChange endId $endId , progress $progress , R.id.add_to_fav ${R.id.add_to_fav} , R.id.add_to_fav ${R.id.transition_add_to_fav}")
//                if(endId == R.id.add_to_fav && progress == 1f) animListener?.invoke(holder.itemView)

            }

            override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                Log.d("ddd", "motion onTransitionCompleted currentId $currentId")

                if(currentId == R.id.add_to_fav) animListener?.invoke(holder.itemView)
            }

            override fun onTransitionTrigger(
                motionLayout: MotionLayout?,
                triggerId: Int,
                positive: Boolean,
                progress: Float
            ) {
                Log.d("ddd", "motion onTransitionChange triggerId $triggerId , positive $positive")

            }
        })
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun updateListData(list: ArrayList<Profile>)
    {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: CellRecyclerViewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(profile:Profile){
            binding.profile = profile
            Glide.with(binding.root.context).asDrawable().load(binding.root.resources.getIdentifier(profile.img, "drawable", binding.root.context.packageName)).into(binding.img)
        }
    }
}