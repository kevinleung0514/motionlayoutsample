package com.example.testing_motionlayout.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testing_motionlayout.R
import com.example.testing_motionlayout.databinding.CellItemBinding

class TestingAdapter : RecyclerView.Adapter<TestingAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),R.layout.cell_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return 30
    }

    class ViewHolder(val binding: CellItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind (){

            Glide.with(binding.root.context).asDrawable().load(ContextCompat.getDrawable(binding.root.context, R.drawable.img_fake_video)).into(binding.img)
        }
    }
}