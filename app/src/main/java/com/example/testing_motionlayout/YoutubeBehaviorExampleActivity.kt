package com.example.testing_motionlayout

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testing_motionlayout.adapter.TestingAdapter
import com.example.testing_motionlayout.databinding.ActivityYoutubeBehaviorExampleBinding

class YoutubeBehaviorExampleActivity : AppCompatActivity() {
    lateinit var mBinding: ActivityYoutubeBehaviorExampleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_youtube_behavior_example)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()

        mBinding.rvVideo.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = TestingAdapter()
        }
    }

}