package com.example.testing_motionlayout

import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testing_motionlayout.adapter.RecyclerViewAnimAdapter
import com.example.testing_motionlayout.data.Profile
import com.example.testing_motionlayout.databinding.ActivityRecyclweviewAnimExampleBinding
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject

class RecyclerViewAnimationExampleActivity: AppCompatActivity() {
    lateinit var mBinding : ActivityRecyclweviewAnimExampleBinding
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_recyclweview_anim_example)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()

        mBinding.rvProfile.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = RecyclerViewAnimAdapter()
            (adapter as RecyclerViewAnimAdapter).animListener = {
                //start animation
                val rect = Rect()
                it.getGlobalVisibleRect(rect)
                Log.d("ddd", "rect $rect")
                val v = View(this@RecyclerViewAnimationExampleActivity)
                v.layoutParams = ConstraintLayout.LayoutParams(100, 100)
                v.y = rect.top.toFloat()
                v.x = rect.right.toFloat()/2
                v.translationZ = 9999f
                v.setBackgroundColor(ContextCompat.getColor(this@RecyclerViewAnimationExampleActivity, R.color.red))
                mBinding.containerRoot.addView(v)

                val anim = TranslateAnimation(rect.left.toFloat(), rect.left.toFloat(), 0f, -rect.top.toFloat())
                anim.fillAfter = true
                anim.duration = 1000
                anim.setAnimationListener(object : Animation.AnimationListener{
                    override fun onAnimationStart(p0: Animation?) {
                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        v.visibility = View.GONE
                        mBinding.containerRoot.removeView(v)
                    }

                    override fun onAnimationRepeat(p0: Animation?) {
                    }
                })
                v.startAnimation(anim)
            }
        }

        updateList()
    }

    private fun updateList() {
        val profileList = arrayListOf<Profile>()
        val inputStream = assets.open("userProfile.json")
        val buffer = ByteArray(inputStream.available())
        inputStream.read(buffer)
        inputStream.close()
        val jsonStr = String(buffer)
        val data = Gson().fromJson(jsonStr, JsonObject::class.java)
        data.getAsJsonArray("data").forEach {
            profileList.add(Gson().fromJson(it, Profile::class.java))
        }

        (mBinding.rvProfile.adapter as RecyclerViewAnimAdapter).updateListData(profileList)
    }
}