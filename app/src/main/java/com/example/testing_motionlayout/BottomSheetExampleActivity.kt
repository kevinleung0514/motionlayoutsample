package com.example.testing_motionlayout

import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testing_motionlayout.adapter.TestingAdapter
import com.example.testing_motionlayout.databinding.ActivityBottomSheetExampleBinding

class BottomSheetExampleActivity: AppCompatActivity() {
    lateinit var mBinding : ActivityBottomSheetExampleBinding
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bottom_sheet_example)
        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()

        mBinding.tvShow.setOnClickListener {
        mBinding.tvShow.text = "fuck u"
            mBinding.fragmentPlaceholder.setProgress(0.5f, 300f)
        }
        mBinding.shippingHistoryList.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = TestingAdapter()
            setOnTouchListener { view, motionEvent ->
                when(motionEvent.action)
                {
                    MotionEvent.ACTION_MOVE -> {
                        mBinding.fragmentPlaceholder.onTouchEvent(motionEvent)
                        false
                    }
                    else -> false
                }
            }
//            addOnScrollListener(object : RecyclerView.OnScrollListener() {
//                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                    super.onScrollStateChanged(recyclerView, newState)
//                    mBinding.fragmentPlaceholder.ontouch
//                }
//
//                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                    super.onScrolled(recyclerView, dx, dy)
//                }
//            })
        }

    }
}