package com.example.testing_motionlayout

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.testing_motionlayout.databinding.ActivityCollapsingToolbarExampleBinding
import com.google.android.material.appbar.AppBarLayout

class CollapsingToolbarExampleActivity: AppCompatActivity() {
    lateinit var mBinding : ActivityCollapsingToolbarExampleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_collapsing_toolbar_example)


        if (!isFinishing()) {
            val window: Window = window
            if (window != null) {
                if (window.decorView != null) window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                window.statusBarColor = Color.TRANSPARENT
            }
        }

        mBinding.lifecycleOwner = this
        mBinding.executePendingBindings()


        val listener = AppBarLayout.OnOffsetChangedListener { unused, verticalOffset ->
            val seekPosition = -verticalOffset / mBinding.appBar.totalScrollRange.toFloat()
            mBinding.motionlayout.collapsingToolbarLayout.progress = seekPosition
        }

        mBinding.appBar.addOnOffsetChangedListener(listener)
    }
}